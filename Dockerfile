FROM weboaks/node-karma-protractor-chrome:debian-node10
ENV DOCKER_CHANNEL=stable \
    DOCKER_VERSION=17.06.0-ce

USER root
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - 
RUN apt-get update 
RUN apt-get install python-pip libpython-dev -y 
RUN pip install --upgrade pip
RUN pip install docker-compose
RUN curl --insecure -fL -o docker.tgz "https://download.docker.com/linux/static/${DOCKER_CHANNEL}/x86_64/docker-${DOCKER_VERSION}.tgz"; \
	tar --extract \
		--file docker.tgz \
		--strip-components 1 \
		--directory /usr/local/bin/ \
	; \
	rm docker.tgz; \
	dockerd -v; \
	docker -v; \
    docker-compose -v

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod 755 /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
CMD ["sh"]
