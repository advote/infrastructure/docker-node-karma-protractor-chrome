# Docker + OracleJDK + Maven container for gitlab ci building [![build status](https://gitlab.com/chvote2/infra/docker-node-karma-protractor-chrome/badges/master/build.svg)](https://gitlab.com/chvote2/infra/docker-node-karma-protractor-chrome/commits/master)

## Instructions
The container is based on an node-karma-protractor-chrome image, suitable for a e2e environment. 
Docker and docker-compose are installed on this base image.

This image is meant to be used in the gitlab-ci pipelines of the CHVote applications.

## Content
* OpenJDK 7u121
* node v7.9.0
* Docker 17.06.00-ce
* Docker-compose: version 1.15.0
